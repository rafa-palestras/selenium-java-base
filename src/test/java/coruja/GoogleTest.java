package coruja;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class GoogleTest {


    private FirefoxDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "/Users/lacerdaph/workspace/palestra-testes/geckodriver");
        this.driver = new FirefoxDriver();
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void deve_fazer_pesquisa_pela_ilha_tecnologia() {

        driver.get("https://google.com/");
        driver.findElement(By.name("q")).sendKeys("ILIA TECNOLOGIA" + Keys.ENTER);
        WebElement firstResult =
                new WebDriverWait(driver, Duration.ofSeconds(10)).until(presenceOfElementLocated(By.cssSelector("h3")));

        Assertions.assertEquals("Ília", firstResult.getAttribute("textContent"));

    }


}
